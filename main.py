import logging
import threading

from pyrogram import idle

from ci import app as bot
from app import app

# 日志记录
logging.basicConfig(level=logging.INFO)
bot.start()
threading.Thread(target=app.run, kwargs={"host": "0.0.0.0", "port": "10111"}, daemon=True).start()
idle()
bot.stop()
